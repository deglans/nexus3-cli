"""Methods to implement upload for specific repository formats (recipes)"""
import os
import pathlib
from pathlib import PurePath

from nexuscli import exception
from nexuscli.api.repository.validations import REMOTE_PATH_SEPARATOR


def upload_maven2(repository, src_file, progress_bar):
    """
    Upload to a maven2 repository.
    """

    def get_gav(path, gav):
        (head_path, tail_path) = os.path.split(path)
        if tail_path == "":
            path = head_path
            (head_path, tail_path) = os.path.split(path)
        (head_gav, tail_gav) = os.path.split(gav)
        v_gav = []
        while True:
            if tail_path != tail_gav:
                v_gav.insert(0, tail_gav)
                gav = head_gav
                (head_gav, tail_gav) = os.path.split(gav)
            else:
                break
        g = '.'.join(v_gav[:len(v_gav)-2])
        a = v_gav[-2]
        v = v_gav[-1]
        return g, a, v

    def get_assets(path, asset_list, g, a, v):
        def get_extension(p):
            ext = pathlib.Path(p).suffix
            if pathlib.Path(p.strip(ext)).suffix == ".tar":
                return f"tar{ext}"
            else:
                return ext.strip(".")

        r_f_assets = {}
        r_d_assets = {}
        r_count = 0
        for asset in asset_list:
            if not (
                    (pathlib.Path(f"{path}/{asset}").suffix == ".md5")
                    or
                    (pathlib.Path(f"{path}/{asset}").suffix == ".sha1")
            ):
                r_count = r_count + 1
                r_d_assets.update({
                    f"maven2.asset{r_count}.extension":
                        get_extension(f"{path}/{asset}")
                })
                r_f_assets.update({
                    f"maven2.asset{r_count}":
                        (PurePath(f"{path}/{asset}").name, open(f"{path}/{asset}", 'rb'))
                })
                tmp = asset
                tmp = tmp.replace(a, "", 1).strip("-")
                tmp = tmp.replace(v, "", 1).strip("-")
                tmp = tmp.replace("." + get_extension(f"{path}/{asset}"), "", 1)
                if len(tmp) != 0:
                    r_d_assets.update({
                        f"maven2.asset{r_count}.classifier":
                            tmp
                    })
        return r_d_assets, r_f_assets, r_count

    for (root, dirs, files) in os.walk(src_file):
        if len(files) != 0:
            (group, artifact, version) = get_gav(src_file, root)
            params = {
                "maven2.generate-pom": "false",
                "maven2.groupId": group,
                "maven2.artifactId": artifact,
                "maven2.version": version
            }
            d_assets, f_assets, num = get_assets(root, files, group, artifact, version)
            for n in range(num):
                next(progress_bar)
            response = repository.nexus_client.http_post(
                'components',
                params={'repository': repository.name, 'Content-Type': 'multipart/form-data'},
                data={**d_assets, **params},
                files=f_assets,
                stream=True
            )
            if response.status_code != 204:
                print(f"params: { {'repository': repository.name, 'Content-Type': 'multipart/form-data'} }")
                print(f"data: { {**d_assets, **params} }")
                print(f"params: { f_assets }")
                raise exception.NexusClientAPIError(
                    f'Uploading to {repository.name}. Reason: {response.reason} '
                    f'Status code: {response.status_code} Text: {response.text}')


def upload_file_npm(repository, src_file, dst_dir=None, dst_file=None):
    """
    Upload a single file to a npm repository.

    :param repository: repository instance used to access Nexus 3 service.
    :type repository: nexuscli.api.repository.model.Repository
    :param src_file: path to the local file to be uploaded.
    :param dst_dir: NOT USED
    :param dst_file: NOT USED
    :raises exception.NexusClientInvalidRepositoryPath: invalid repository
        path.
    :raises exception.NexusClientAPIError: unknown response from Nexus API.
    """
    params = {'repository': repository.name}
    files = {'npm.asset': (PurePath(src_file).name, open(src_file, 'rb'))}

    response = repository.nexus_client.http_post(
        'components', files=files, params=params, stream=True)

    if response.status_code != 204:
        raise exception.NexusClientAPIError(
            f'Uploading to {repository.name}. Reason: {response.reason} '
            f'Status code: {response.status_code} Text: {response.text}')


def upload_file_pypi(repository, src_file, dst_dir=None, dst_file=None):
    """
    Upload a single file to a PyPI repository.

    :param repository: repository instance used to access Nexus 3 service.
    :type repository: nexuscli.api.repository.model.Repository
    :param src_file: path to the local file to be uploaded.
    :param dst_dir: NOT USED
    :param dst_file: NOT USED
    :raises exception.NexusClientInvalidRepositoryPath: invalid repository
        path.
    :raises exception.NexusClientAPIError: unknown response from Nexus API.
    """
    params = {'repository': repository.name}
    files = {'pypi.asset': (PurePath(src_file).name, open(src_file, 'rb'))}

    response = repository.nexus_client.http_post(
        'components', files=files, params=params, stream=True)

    if response.status_code != 204:
        raise exception.NexusClientAPIError(
            f'Uploading to {repository.name}. Reason: {response.reason} '
            f'Status code: {response.status_code} Text: {response.text}')


def upload_file_raw(repository, src_file, dst_dir, dst_file):
    """
    Upload a single file to a raw repository.

    :param repository: repository instance used to access Nexus 3 service.
    :type repository: nexuscli.api.repository.model.Repository
    :param src_file: path to the local file to be uploaded.
    :param dst_dir: directory under dst_repo to place file in. When None,
        the file is placed under the root of the raw repository
    :param dst_file: destination file name.
    :raises exception.NexusClientInvalidRepositoryPath: invalid repository
        path.
    :raises exception.NexusClientAPIError: unknown response from Nexus API.
    """
    dst_dir = os.path.normpath(dst_dir or REMOTE_PATH_SEPARATOR)

    params = {'repository': repository.name}
    files = {'raw.asset1': open(src_file, 'rb').read()}
    data = {
        'raw.directory': dst_dir,
        'raw.asset1.filename': dst_file,
    }

    response = repository.nexus_client.http_post(
        'components', files=files, data=data, params=params, stream=True)

    if response.status_code != 204:
        raise exception.NexusClientAPIError(
            f'Uploading to {repository.name}. Reason: {response.reason} '
            f'Status code: {response.status_code} Text: {response.text}')


def upload_file_yum(repository, src_file, dst_dir, dst_file):
    """
    Upload a single file to a yum repository.

    :param repository: repository instance used to access Nexus 3 service.
    :type repository: nexuscli.api.repository.model.Repository
    :param src_file: path to the local file to be uploaded.
    :param dst_dir: directory under dst_repo to place file in.
    :param dst_file: destination file name.
    :raises exception.NexusClientAPIError: unknown response from Nexus API.
    """
    dst_dir = dst_dir or REMOTE_PATH_SEPARATOR
    repository_path = REMOTE_PATH_SEPARATOR.join(
        ['repository', repository.name, dst_dir, dst_file])

    with open(src_file, 'rb') as fh:
        response = repository.nexus_client.http_put(
            repository_path, data=fh, stream=True,
            service_url=repository.nexus_client.config.url)

    if response.status_code != 200:
        raise exception.NexusClientAPIError(
            f'Uploading to {repository_path}. Reason: {response.reason} '
            f'Status code: {response.status_code} Text: {response.text}')
